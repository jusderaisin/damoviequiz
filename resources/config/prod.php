<?php
define("ROOT_PATH", __DIR__ . "/../..");

$app['log.level'] = Monolog\Logger::ERROR;
$app['api.version'] = "v1";
$app['api.endpoint'] = "/api";
$app['redis.host'] = 'tcp://127.0.0.1:6379';

/**
 * SQLite database file
 */
/*
$app['db.options'] = array(
    'driver' => 'pdo_sqlite',
    'path' => realpath(ROOT_PATH . '/app.db'),
);
*/

/**
 * MySQL
 */
$app['db.options'] = array(
    "driver" => "pdo_mysql",
    "user" => "root",
    "password" => "root",
    "dbname" => "silex_api",
    "dbhost" => "localhost",
);
