<?php

namespace App\Controllers;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class NotesController
{

    protected $notesService;

    public function __construct($service)
    {
        $this->notesService = $service;
    }

    public function getOne($id)
    {
        $note = $this->notesService->getOne($id);
        if($note) {
            return new JsonResponse($note);
        }else{
            return new JsonResponse(array('message' => 'Note not found'), 404);
        }
    }

    public function getAll()
    {
        return new JsonResponse($this->notesService->getAll());
    }

    public function save(Request $request)
    {
        if($request->request->get("note"))
            return new JsonResponse(array("id" => $this->notesService->save($this->getDataFromRequest($request))));
        else
            return new JsonResponse(array("message" => "Missing parameter note"), 201);

    }

    public function update($id, Request $request)
    {
        $note = $this->getDataFromRequest($request);
        $this->notesService->update($id, $note);
        return new JsonResponse($note);

    }

    public function delete($id)
    {

        return new JsonResponse($this->notesService->delete($id));

    }

    public function getDataFromRequest(Request $request)
    {
        return $note = array(
            "note" => $request->request->get("note")
        );
    }
}
