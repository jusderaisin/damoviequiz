<?php

namespace App\Controllers;


use App\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class GamesController
{

    protected $gamesService;
    protected $validatorService;


    public function __construct($gamesService, $validatorService)
    {
        $this->gamesService = $gamesService;
        $this->validatorService = $validatorService;
    }

    public function start(Request $request)
    {
        $email = $request->request->get('email');
        
        $user = new User();
        $user->setEmail($email);
        
        $errors = $this->validatorService->validate($user);

        if(count($errors) > 0){
            return new JsonResponse(array('message' => 'Email format not valid'), Response::HTTP_BAD_REQUEST);
        }else{
            return new JsonResponse($this->gamesService->start($user), Response::HTTP_CREATED);
        }
    }

    public function getStat(Request $request)
    {
        $email = $request->query->get('email');

        if(empty($email)){
            return new JsonResponse(array('message' => 'Email required'), Response::HTTP_BAD_REQUEST);
        }

        $user = new User();
        $user->setEmail($email);
        $errors = $this->validatorService->validate($user);

        if(count($errors) > 0){
            return new JsonResponse(array('message' => 'Email format not valid'), Response::HTTP_BAD_REQUEST);
        }else{
            $user = $this->gamesService->getStat($user);
            if($user == null)
                return new JsonResponse(array('message' => 'User not exist, please start a game with your email first'), Response::HTTP_NOT_FOUND);
            else
                return new JsonResponse($user);
        }
    }
}