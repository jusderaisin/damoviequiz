<?php


namespace App\Controllers;


use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

class MoviesController
{
    protected $moviesService;

    public function __construct($moviesService)
    {
        $this->moviesService = $moviesService;
    }


    public function indexMovies()
    {
        return new JsonResponse($this->moviesService->indexMovies());
    }

    public function getRandOne()
    {
        return new JsonResponse($this->moviesService->getRandOne());
    }


    public function guessMovieActor(Request $request)
    {
        $imdbId = $request->request->get('id');
        $actor = $request->request->get('actor');
        $email = $request->request->get('email');

        if(empty($imdbId) || empty($actor) || empty($email)){
            return new JsonResponse(array('message' => 'Errors in parameters. All fields are required'), Response::HTTP_BAD_REQUEST);
        }

        try{
            return new JsonResponse($this->moviesService->guessMovieActor($imdbId, $actor, $email));
            
        }catch (NotFoundResourceException $ex){
            return new JsonResponse(array('message' => 'Resources not found'), Response::HTTP_NOT_FOUND);
        }
    }
}