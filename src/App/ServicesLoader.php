<?php

namespace App;

use Silex\Application;

class ServicesLoader
{
    protected $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function bindServicesIntoContainer()
    {
        $this->app['notes.service'] = function() {
            return new Services\NotesService($this->app["db"]);
        };
        $this->app['movies.service'] = function() {
            return new Services\MoviesService($this->app['predis'], $this->app['guzzle']);
        };
        $this->app['games.service'] = function() {
            return new Services\GamesService($this->app['predis']);
        };
    }
}

