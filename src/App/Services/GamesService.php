<?php


namespace App\Services;

use App\Entity\User;

class GamesService
{

    protected $predis;

    public function __construct($predis)
    {
        $this->predis = $predis;
    }


    public function start(User $user)
    {

        $this->predis->hmset('user:'.$user->getEmail(), array(
            'email' => $user->getEmail(),
            'score' => $user->getScore(),
            'bestScore' => $user->getBestScore(),
            'continue' => $user->isContinue()
        ));
     
        return $user;
    }

    public function getStat(User $user)
    {
        if(!$this->predis->exists('user:'.$user->getEmail()))
            return null;
        $infos = $this->predis->hvals('user:'.$user->getEmail());

        $user->setScore($infos[1]);
        $user->setBestScore($infos[2]);
        $user->setContinue($infos[3]);

        return $user;
    }

}