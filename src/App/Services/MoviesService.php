<?php


namespace App\Services;


use App\Entity\Movie;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

class MoviesService
{
    const NB_MOVIE_INDEXED = 10;

    protected $guzzle;
    protected $predis;

    public function __construct($predis, $guzzle)
    {
        $this->predis = $predis;
        $this->guzzle = $guzzle;
    }


    public function getRandOne()
    {
        $movieIds = $this->predis->executeRaw(array('keys', 'movie:*'));

        //get one movie randomly
        $index = array_rand($movieIds, 1);
        $id = $movieIds[$index];

        //remove this movie from array
        unset($movieIds[$index]);

        // create a movie entity
        $movie = new Movie();
        $movie->setId($id);
        $movie->setTitle($this->predis->hget($id, 'title'));
        $movie->setActors($this->getRandActor($id));

        //get 4 actors name in 4 other movies
        $indexes = array_rand($movieIds, 4);
        foreach ($indexes as $index){
            $actor = $this->getRandActor($movieIds[$index]);
            $movie->setActors($movie->getActors().', '.$actor);
        }

        return $movie;
    }


    private function getRandActor($movieId)
    {
        $actors = $this->predis->hget($movieId, 'actors');
        $tab = explode(',', $actors);
        $index = array_rand($tab, 1);
        return trim($tab[$index]);
    }

    public function indexMovies()
    {
        $this->indexImdbIds();

        $movies = $this->predis->executeRaw(array('keys', 'movie:*'));
        foreach ($movies as $movie) {
            $tab = explode(':', $movie);
            $imdbId = $tab[1];

            $res = $this->guzzle->get('http://www.omdbapi.com/?i=' . $imdbId . '&plot=short&r=json');
            $json = json_decode($res->getBody());

            if (!empty($json) && !empty($json->imdbID)) {
                $this->predis->hmset('movie:' . $json->imdbID, array(
                        'actors' => $json->Actors)
                );
            }
        }
        return Response::HTTP_CREATED;
    }

    private function indexImdbIds()
    {
        for($i = 100; $i < 100 + self::NB_MOVIE_INDEXED; $i++){
            $res = $this->guzzle->get('https://api.themoviedb.org/3/movie/'.$i.'?api_key=74be26012eb788fef7a49032161efa59');
            if($res->getStatusCode() == 200){
                $obj = json_decode($res->getBody());

                if(!empty($obj->imdb_id) && !empty($obj->title)){
                    $this->predis->hmset('movie:'.$obj->imdb_id, array(
                            'title' => $obj->title)
                    );
                }
            }
        }
    }

    public function guessMovieActor($imdbId, $actor, $email)
    {
        $movie = $this->predis->exists($imdbId);
        $user = $this->predis->exists('user:'.$email);
        //if user key not exists. must start a game before
        if(!$user || !$movie){
            throw new NotFoundResourceException;
        }

        $actors = $this->predis->hget($imdbId, 'actors');
        $tab = explode(', ', $actors);
        $score = $this->predis->hget('user:'.$email, 'score');


        //find actor
        if(in_array($actor, $tab)){
            if($score >= 0){
                $this->predis->hmset('user:'.$email, array(
                    'score' => $score+1,
                    'continue' => true
                    ));
            }
            return array('message' => 'ok');
        }else{
            $bestScore = $this->predis->hget('user:'.$email, 'bestScore');
            if($bestScore == null || $bestScore < $score){
                $bestScore = $score;
            }
            $this->predis->hmset('user:'.$email, array(
                'bestScore' => $bestScore,
                'continue' => 0, //game is finish
                'score' => 0 //reset player score
            ));
            return array('message' => 'ko');
        }
    }

}