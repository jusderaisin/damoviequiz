<?php

namespace App\Entity;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;

class User
{

    public $email;
    public $score;
    public $bestScore;
    public $continue;

    static public function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('email', new Assert\Email());
        $metadata->addPropertyConstraint('email', new Assert\NotBlank());
    }

    public function __construct()
    {
        $this->score = 0;
        $this->bestScore = 0;
        $this->continue =  true;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return int
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * @param int $score
     */
    public function setScore($score)
    {
        $this->score = $score;
    }

    /**
     * @return mixed
     */
    public function getBestScore()
    {
        return $this->bestScore;
    }

    /**
     * @param mixed $bestScore
     */
    public function setBestScore($bestScore)
    {
        $this->bestScore = $bestScore;
    }

    /**
     * @param boolean $continue
     */
    public function setContinue($continue)
    {
        $this->continue = $continue;
    }

    /**
     * @return boolean
     */
    public function isContinue()
    {
        return $this->continue;
    }

}