<?php

namespace App;

use Silex\Application;

class RoutesLoader
{
    private $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->instantiateControllers();

    }

    private function instantiateControllers()
    {
        $this->app['notes.controller'] = function() {
            return new Controllers\NotesController($this->app['notes.service']);
        };
        $this->app['movies.controller'] = function() {
            return new Controllers\MoviesController($this->app['movies.service']);
        };
        $this->app['games.controller'] = function() {
            return new Controllers\GamesController($this->app['games.service'], $this->app['validator']);
        };
    }

    public function bindRoutesToControllers()
    {
        $api = $this->app["controllers_factory"];

        $api->get('/notes', "notes.controller:getAll");
        $api->get('/notes/{id}', "notes.controller:getOne");
        $api->post('/notes', "notes.controller:save");
        $api->put('/notes/{id}', "notes.controller:update");
        $api->delete('/notes/{id}', "notes.controller:delete");

        $api->get('/movies', 'movies.controller:indexMovies');
        $api->get('/movie', 'movies.controller:getRandOne');
        $api->post('/movie', 'movies.controller:guessMovieActor');

        $api->post('/games', 'games.controller:start');
        $api->get('/games', 'games.controller:getStat');


        $this->app->mount($this->app["api.endpoint"].'/'.$this->app["api.version"], $api);
    }
}

