<?php

namespace Tests\Services;



use Silex\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Silex\Application;

class MoviesServiceTest extends WebTestCase
{

    //because of the same php process, we have the error : already define const
    protected $preserveGlobalState = FALSE;
    protected $runTestInSeparateProcess = TRUE;

    /**
     * Creates the application.
     *
     * @return HttpKernelInterface
     */
    public function createApplication()
    {
        $app = new Application();
        require __DIR__ . '/../../resources/config/prod.php';
        require __DIR__ . '/../../src/app.php';
        return $this->app = $app;
    }
    
    public function testGetOneMovie()
    {
        $client = $this->createClient();
        $crawler = $client->request('GET', '/api/v1/movie');

        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertTrue($response->isOk());
    }
    

    public function testGuessMovie()
    {
        $client = $this->createClient();

        $client->request('POST', '/api/v1/movie');
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());

        $param = array(
            'email' => 'notexist@gmail.com',
            'id' => 'movie:notexist',
            'actor' => 'name'
        );
        $client->request('POST', '/api/v1/movie', [], [], array('CONTENT_TYPE' => 'application/json'), json_encode($param));
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_NOT_FOUND, $response->getStatusCode());
    }


}