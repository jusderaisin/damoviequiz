<?php


namespace Tests\Services;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Silex\Application;
use Silex\WebTestCase;

class GamesServiceTest extends WebTestCase
{
//because of the same php process, we have the error : already define const
    protected $preserveGlobalState = FALSE;
    protected $runTestInSeparateProcess = TRUE;

    /**
     * Creates the application.
     *
     * @return HttpKernelInterface
     */
    public function createApplication()
    {
        $app = new Application();
        require __DIR__ . '/../../resources/config/prod.php';
        require __DIR__ . '/../../src/app.php';
        return $this->app = $app;
    }

    public function testStartGame()
    {
        $client = $this->createClient();

        $client->request('POST', '/api/v1/games');
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());

        $param = array('email' => 'test@test.com');
        $client->request('POST', '/api/v1/games', [], [], array('CONTENT_TYPE' => 'application/json'), json_encode($param));
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
    }

    public function testUserStat()
    {
        $client = $this->createClient();
        $client->request('GET', '/api/v1/games');
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());

        $client->request('GET', '/api/v1/games?email=notexist@gmail.com');
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_NOT_FOUND, $response->getStatusCode());

        $client->request('GET', '/api/v1/games?email=test@test.com');
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }
}